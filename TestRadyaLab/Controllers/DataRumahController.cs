﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestRadyaLab.Models;

namespace TestRadyaLab.Controllers
{
    public class DataRumahController : Controller
    {
        ContextModel context = new ContextModel();
        //
        // GET: /DataRumah/
        
        public ActionResult Index()
        {
            ViewBag.listData = context.Rumah.ToList();
            return View();
        }

        public ActionResult Add()
        {
            Rumah model = new Rumah();
            ViewBag.Title = "Tambah Data Rumah";
            return View("Form", model);
        }

        [HttpPost]
        public ActionResult Add(Rumah model)
        {
            context.Rumah.Add(model);
            context.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            Rumah model = context.Rumah.Find(id);
            ViewBag.Title = "Edit " + model.Alamat;
            return View("Form", model);
        }

        [HttpPost]
        public ActionResult Edit(Rumah model)
        {
            context.Rumah.Attach(model);

            var entry = context.Entry(model);
            
            entry.State = System.Data.Entity.EntityState.Modified;

            context.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            Rumah dbitem = context.Rumah.Find(id);

            context.Rumah.Remove(dbitem);

            context.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}
