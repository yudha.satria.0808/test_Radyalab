﻿using System;
using System.Web;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestRadyaLab.Models
{
    public class Rumah
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required(ErrorMessage="Tipe harus diisi")]
        public string Tipe { get; set; }
        [Required(ErrorMessage="Alamat harus diisi")]
        public string Alamat { get; set; }
        [Required(ErrorMessage="Latitude harus diisi")]
        public string Lat { get; set; }
        [Required(ErrorMessage="Longitude harus diisi")]
        public string Long { get; set; }
        public string Kecamtan { get; set; }
        public string Kota { get; set; }
        public string Provinsi { get; set; }
    }
}